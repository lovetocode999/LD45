var brightIncrease = false;
var brightness = 100;
var time = 0;
var backdropNum = 0;
var credits = false;
var info = true;
var paused = false;
var leftUp = false;
var leftDown = false;
var rightUp = false;
var rightDown = false;
var leftHopStarted = false;
var rightHopStarted = false;
var gameFinished = false;
var lastFrameTime = Date.now();
var speed = 1;
var score = 0;
var criminalX = 0;
var highscore = !!localStorage.getItem('highscore') ? localStorage.getItem('highscore') : 0;
document.getElementById('highscore').innerHTML = 'Highscore: $'+(highscore*100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
var then = Date.now();
var clones = [];
var gameStarted = false;
var gameRunning = false;
var ypx = screen.height/360;
var xpx = screen.width/480;
let stage = new blockLike.Stage({parent: document.getElementById('stage'), backdrop: new blockLike.Backdrop({image: 'Images/background.svg'}), sensing: true});
stage.addBackdrop(new blockLike.Backdrop({image: 'Images/backgroundBankless.svg'}));
for (i = 2; i < 17; i++) {
  stage.addBackdrop(new blockLike.Backdrop({image: 'Images/backgroundBankless'+i+'.svg'}));
}
for (i = 16; i > 1; i--) {
  stage.addBackdrop(new blockLike.Backdrop({image: 'Images/backgroundBankless'+i+'.svg'}));
}
let playButton = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/play.svg'})});
playButton.addTo(stage);
playButton.setSize(120*ypx);
spriteGoTo(playButton, -130, 0);
let infoButton = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/info.svg'})});
infoButton.addTo(stage);
infoButton.setSize(120*ypx);
let creditButton = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/credit.svg'})});
creditButton.addTo(stage);
creditButton.setSize(120*ypx);
spriteGoTo(creditButton, 130, 0)
let explosionSprite = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/explosion.svg'})});
explosionSprite.addTo(stage);
explosionSprite.setSize(300*ypx);
spriteGoTo(explosionSprite, 130, 0);
explosionSprite.hide();
let triggerSprite = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/trigger.svg'})});
for (i = 2; i < 8; i++) {
  triggerSprite.addCostume(new blockLike.Costume({image: 'Images/trigger'+i+'.svg'}));
}
triggerSprite.addTo(stage);
triggerSprite.setSize(120*ypx);
spriteGoTo(triggerSprite, -150, -75);
triggerSprite.hide();
let pressSpace = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/pressSpace.svg'})});
pressSpace.addTo(stage);
pressSpace.setSize(480*ypx);
spriteGoTo(pressSpace, 0, 100);
pressSpace.hide();
let criminal = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/criminal.svg'})});
criminal.addCostume(new blockLike.Costume({image: 'Images/criminalTrigger.svg'}));
for (i = 2; i < 8; i++) {
  criminal.addCostume(new blockLike.Costume({image: 'Images/criminalTrigger'+i+'.svg'}));
}
for (i = 2; i < 6; i++) {
  criminal.addCostume(new blockLike.Costume({image: 'Images/criminal'+i+'.svg'}));
}
criminal.setRotationStyle('left-right');
criminal.costumeNum = 0;
criminal.setSize(100*ypx);
criminal.addTo(stage);
spriteGoTo(criminal, -150, -75);
criminal.hide();
let objects = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/money.svg'})});
objects.addCostume(new blockLike.Costume({image: 'Images/fire.svg'}));
objects.addCostume(new blockLike.Costume({image: 'Images/getMoney.svg'}));
objects.addCostume(new blockLike.Costume({image: 'Images/duck.svg'}));
objects.addCostume(new blockLike.Costume({image: 'Images/gem.svg'}));
objects.addCostume(new blockLike.Costume({image: 'Images/time.svg'}));
objects.setSize(50*ypx);
objects.addTo(stage);
objects.hide();
let gameOver = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/gameOver.svg'})});
gameOver.setSize(480*ypx);
spriteGoTo(gameOver, 0, 100);
gameOver.addTo(stage);
gameOver.hide();
let bankbuster = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/bankbusters.svg'})});
bankbuster.setSize(480*ypx);
spriteGoTo(bankbuster, 0, 100);
bankbuster.addTo(stage);
stage.sendSpriteToBack(bankbuster);
let criminalHitbox = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/hitbox.svg'})});
criminalHitbox.addTo(stage);
criminalHitbox.setSize(80*ypx);
criminalHitbox.hide();
criminalHitbox.direction = 90;
let menuButton = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/menu.svg'})});
menuButton.addTo(stage);
menuButton.setSize(120*ypx);
spriteGoTo(menuButton, -125, -100);
menuButton.hide()
let playAgain = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/playAgain.svg'})});
playAgain.addTo(stage);
playAgain.setSize(120*ypx);
spriteGoTo(playAgain, 0, -100);
playAgain.hide();
let pauseButton = new blockLike.Sprite({costume: new blockLike.Costume({image: 'Images/pause.svg'})});
pauseButton.addCostume(new blockLike.Costume({image: 'Images/unpause.svg'}));
pauseButton.setSize(20*ypx);
spriteGoTo(pauseButton, 200, 130);
pauseButton.addTo(stage);
pauseButton.hide();

playButton.whenEvent('mouseover', function(e){
  this.setSize(180*ypx);
});

infoButton.whenEvent('mouseover', function(e){
  this.setSize(180*ypx);
});

creditButton.whenEvent('mouseover', function(e){
  this.setSize(180*ypx);
});

menuButton.whenEvent('mouseover', function(e){
  this.setSize(180*ypx);
});

pauseButton.whenEvent('mouseover', function(e) {
  this.setSize(30*ypx);
});

playAgain.whenEvent('mouseover', function(e){
  this.setSize(180*ypx);
});

playButton.whenEvent('mouseout', function(e){
  this.setSize(120*ypx);
});

infoButton.whenEvent('mouseout', function(e){
  this.setSize(120*ypx);
});

creditButton.whenEvent('mouseout', function(e){
  this.setSize(120*ypx);
});

menuButton.whenEvent('mouseout', function(e){
  this.setSize(120*ypx);
});

playAgain.whenEvent('mouseout', function(e){
  this.setSize(120*ypx);
});

pauseButton.whenEvent('mouseout', function(e) {
  this.setSize(20*ypx);
});

playButton.whenClicked(function(){
  this.setSize(240*ypx);
  this.css('filter', 'brightness(3)');
  this.wait(0.1);
  this.setSize(120*ypx);
  this.css('filter', 'brightness(1)');
  beginGame();
});

infoButton.whenClicked(function(){
  this.setSize(240*ypx);
  this.css('filter', 'brightness(3)');
  this.wait(0.1);
  this.setSize(120*ypx);
  this.css('filter', 'brightness(1)');
  document.getElementById('intro').style.visibility = 'visible';
  info = true;
});

creditButton.whenClicked(function(){
  this.setSize(240*ypx);
  this.css('filter', 'brightness(3)');
  this.wait(0.1);
  this.setSize(120*ypx);
  this.css('filter', 'brightness(1)');
  document.getElementById('credits').style.visibility = 'visible';
  credits = true;
});

document.onclick = function() {
  if (info) {
    document.getElementById('intro').style.visibility = 'hidden';
    info = false;
  } else if (credits) {
    document.getElementById('credits').style.visibility = 'hidden';
    credits = false;
  }
};

menuButton.whenClicked(function(){
  this.setSize(240*ypx);
  this.css('filter', 'brightness(3)');
  this.wait(0.1);
  this.setSize(120*ypx);
  this.css('filter', 'brightness(1)');
  document.location.href = document.location.href;
});

playAgain.whenClicked(function(){
  triggerSprite.playSoundLoop('Sounds/music.wav');
  this.setSize(240*ypx);
  this.css('filter', 'brightness(3)');
  this.wait(0.1);
  this.setSize(120*ypx);
  this.css('filter', 'brightness(1)');
  restart();
});

pauseButton.whenClicked(function() {
  this.setSize(40*ypx);
  this.css('filter', 'brightness(3)');
  this.wait(0.1);
  this.setSize(30*ypx);
  this.css('filter', 'brightness(1)');
  paused = !paused;
  pauseButton.nextCostume();
  !paused && gameLoop();
  document.getElementById('pauseDiv').style.visibility = paused ? 'visible' : 'hidden';
  pauseButton.css('z-index', 3);
});

document.getElementById('pauseDiv').onclick = function() {
  paused = !paused;
  pauseButton.nextCostume();
  gameLoop();
  document.getElementById('pauseDiv').style.visibility = 'hidden';
};

function beginGame() {
  playButton.hide();
  infoButton.hide();
  creditButton.hide();
  bankbuster.hide()
  criminal.show();
  criminal.switchCostumeToNum(1);
  startGameLoop();
}

function startGameLoop() {
  document.getElementById('score').style.visibility = 'visible';
  document.getElementById('highscore').style.visibility = 'visible';
  triggerSprite.playSoundLoop('Sounds/music.wav');
  pressSpace.show();
  gameStarted = true;
}

triggerSprite.whenKeyPressed(' ', function() {
  if(gameStarted && !gameRunning && !gameFinished) {
    for(i = 1; i < 7; i++) {
      this.switchCostumeToNum(i);
      criminal.switchCostumeToNum(i+1);
      this.wait(0.1);
    }
    explosionSprite.show();
    this.stopSounds();
    this.playSoundUntilDone('Sounds/boom.wav');
    this.playSoundLoop('Sounds/music.wav');
    for(i = 6; i > 1; i--) {
      this.switchCostumeToNum(i);
      criminal.switchCostumeToNum(i+1);
      this.wait(0.05);
    }
    this.switchCostumeToNum(0);
    criminal.switchCostumeToNum(1);
    this.wait(0.5);
    explosionSprite.hide();
    stage.switchBackdropToNum(1);
    pressSpace.hide();
    triggerSprite.hide();
    gameRunning = true;
    firstFrame();
  }
});

function firstFrame() {
  stage.switchBackdropToNum(1);
  backdropNum = 1;
  pauseButton.show();
  createObject();
  updateObjects();
  lastFrameTime = Date.now()
  spriteGoTo(criminal, 0, -75);
  criminal.switchCostumeToNum(0);
  gameLoop();
}

criminal.whenKeyPressed('a', function() {
  if (gameRunning && !criminal.rightHop) {
    criminal.leftHop = true;
    criminal.direction = -90;
    criminal.pointInDirection(-90);
  }
});

criminal.whenKeyPressed(37, function() {
  if (gameRunning && !criminal.rightHop) {
    criminal.leftHop = true;
    criminal.direction = -90;
    criminal.pointInDirection(-90);
  }
});

criminal.whenKeyPressed('d', function() {
  if (gameRunning && !criminal.leftHop) {
    criminal.rightHop = true;
    criminal.direction = 90;
    criminal.pointInDirection(90);
  }
});

criminal.whenKeyPressed(39, function() {
  if (gameRunning && !criminal.leftHop) {
    criminal.rightHop = true;
    criminal.direction = 90;
    criminal.pointInDirection(90);
  }
});

function gameLoop() {
  moveCharacter();
  updateScore();
  if (time == 100) {
    if (brightness == 100) {
      brightness--;
    } else if (brightness == 50) {
      brightness++;
    } else if (brightIncrease) {
      brightness++;
    } else {
      brightness--;
    }
    document.body.style.filter = 'brightness('+brightness/100+')';
    time = 0;
  }
  time++;
  var now = Date.now();
  if (now - then > ((1000/(1000/(now-lastFrameTime)))*900)/speed) {
    then = now;
    createObject();
  }
  updateObjects();
  if (gameRunning) {
    lastFrameTime = Date.now()
    if (!paused) {
      window.requestAnimationFrame(gameLoop);
    }
  } else {
    deleteAllClones()
  }
}

function createObject() {
  if (Math.floor(Math.random()*30) == 1) {
    var object = Math.floor(Math.random()*3)+3;
  } else {
    var object = Math.floor(Math.random()*2);
  }
  objects.switchCostumeToNum(object);
  objects.costumeNum = object;
  objects.newDirection = Math.floor(Math.random()*2)-1;
  clones.push(objects.clone());
}

function updateObjects() {
  for (i = 0; i < clones.length; i++) {
    var clone = clones[i]
    if (clone.y < -75*ypx && clone.isTouching(criminalHitbox)) {
      clone.removeFrom(stage);
      if (!!document.getElementById(clone.id)) document.getElementById(clone.id).remove();;
      delete clones[i];
      clones.splice(i, 1);
      var imgs = document.getElementsByTagName('img');
      for (i = 0; i < imgs.length; i++) {
        removeExtraImages();
      }
    }
    if (clone.y < -110*ypx) {
      clone.removeFrom(stage);
      if (!!document.getElementById(clone.id)) document.getElementById(clone.id).remove();
      delete clones[i];
      clones.splice(i, 1);
      var imgs = document.getElementsByTagName('img');
      for (i = 0; i < imgs.length; i++) {
        removeExtraImages();
      }
    }
    stage.sendSpriteToFront(clone);
    clone.turnRight(clone.newDirection*1);
    clone.changeY(-1*speed);
    if (clone.isTouching(criminalHitbox)) {
      if (clone.costumeNum == 0) {
        clone.switchCostumeToNum(2);
        if(!clone.soundPlayed) {
          clone.soundPlayed = true;
          triggerSprite.playSound('Sounds/money.wav');
          score++;
          speed++;
        }
      } else if (clone.costumeNum == 3) {
        clone.switchCostumeToNum(2);
        if(!clone.soundPlayed) {
          clone.soundPlayed = true;
          triggerSprite.playSound('Sounds/money.wav');
          score+=10;
          speed++;
        }
      } else if (clone.costumeNum == 4) {
        clone.switchCostumeToNum(2);
        if(!clone.soundPlayed) {
          clone.soundPlayed = true;
          triggerSprite.playSound('Sounds/money.wav');
          score+=100;
          speed++;
        }
      } else if (clone.costumeNum == 5) {
        clone.switchCostumeToNum(2);
        if(!clone.soundPlayed) {
          clone.soundPlayed = true;
          triggerSprite.playSound('Sounds/money.wav');
          speed-=5;
          if (speed < 1) {
            speed = 1;
          }
        }
      } else {
        brightness = 100;
        document.body.style.filter = 'brightness(1)';
        pauseButton.hide();
        menuButton.show();
        playAgain.show();
        gameFinished = true;
        triggerSprite.stopSounds();
        triggerSprite.playSound('Sounds/gameOver.wav');
        document.getElementById('largeScore').style.visibility = 'visible';
        document.getElementById('largeHighscore').style.visibility = 'visible'
        document.getElementById('largeScore').innerHTML = 'Score: $'+(score*100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        document.getElementById('largeHighscore').innerHTML = 'Highscore: $'+(highscore*100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        document.getElementById('score').style.visibility = 'hidden';
        document.getElementById('highscore').style.visibility = 'hidden';
        criminal.hide();
        gameOver.show();
        deleteAllClones();
        gameRunning = false;
      }
    }
  }
  criminalHitbox.goTo(criminal.x, criminal.y-(10*ypx));
}

function deleteAllClones() {
  for (i = 0; i < clones.length; i++) {
    clones[i].removeFrom(stage);
    if (!!document.getElementById(clones[i].id)) document.getElementById(clones[i].id).remove();
    delete clones[i];
    clones.splice(i, 1);
  }
  var imgs = document.getElementsByTagName('img');
  for (i = 0; i < imgs.length+1; i++) {
    removeExtraImages();
  }
}

objects.whenCloned(function() {
  this.cloneNum = clones.length;
  this.addTo(stage);
  this.show();
  spriteGoTo(this, Math.floor(Math.random()*480)-240, 170);
});

function spriteGoTo(sprite,x,y){
  sprite.goTo(xpx*x, ypx*y);
}

function updateScore() {
  document.getElementById('score').innerHTML = 'Score: $'+(score*100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  if (score > highscore) {
    highscore = score;
    localStorage.setItem('highscore', highscore);
    document.getElementById('highscore').innerHTML = 'Highscore: $'+(highscore*100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}

function removeExtraImages() {
  var img = document.getElementsByTagName('img');
  for (i = 0; i < img.length; i++) {
    if (/.{1,100}\/Images\/fire.svg/gi.test(img[i].src) || /.{1,100}\/Images\/money.svg/gi.test(img[i].src)) {
      var idArray = [];
      for (j = 0; j < clones.length; j++) {
        idArray.push(clones[j].id);
      }
      if (!idArray.includes(img[i].parentElement.id)) {
        img[i].remove();
      }
    }
  }
}

function moveCharacter() {
  if (criminal.leftHop) {
    if (!leftHopStarted && criminal.costumeNum !== 0) {
      leftHopStarted = true;
    }
    if (criminal.costumeNum == 0) {
      leftUp = true;
      criminal.costumeNum = 8;
      criminal.switchCostumeToNum(8);
      criminal.changeX(-3*xpx);
      if (criminal.x < -240*xpx) spriteGoTo(criminal, 240, -75);
      return;
    }
    if (leftUp) {
      if (criminal.costumeNum < 12) {
        criminal.costumeNum++;
        criminal.switchCostumeToNum(criminal.costumeNum);
        criminal.changeX(-3*xpx);
        if (criminal.x < -240*xpx) spriteGoTo(criminal, 240, -75);
        return;
      } else if (criminal.costumeNum == 12) {
        leftUp = false;
        leftDown = true;
        criminal.costumeNum = 11;
        criminal.switchCostumeToNum(11);
        criminal.changeX(-3*xpx);
        if (criminal.x < -240*xpx) spriteGoTo(criminal, 240, -75);
        return;
      }
    }
    if (leftDown) {
      if (criminal.costumeNum > 8) {
        criminal.costumeNum--;
        criminal.switchCostumeToNum(criminal.costumeNum);
        criminal.changeX(-3*xpx);
        if (criminal.x < -240*xpx) spriteGoTo(criminal, 240, -75);
        return;
      } else if (criminal.costumeNum == 8) {
        leftHopStarted = false;
        criminal.leftHop = false;
        leftDown = false;
        criminal.costumeNum = 0;
        criminal.switchCostumeToNum(0);
        criminal.changeX(-3*xpx);
        if (criminal.x < -240*xpx) spriteGoTo(criminal, 240, -75);
        return;
      }
    }
  } else if (criminal.rightHop) {
    if (!rightHopStarted && criminal.costumeNum !== 0) {
      rightHopStarted = true;
    }
    if (criminal.costumeNum == 0) {
      rightUp = true;
      criminal.costumeNum = 8;
      criminal.switchCostumeToNum(8);
      criminal.changeX(2*xpx);
      if (criminal.x > 240*xpx) spriteGoTo(criminal, -240, -75);
      return;
    }
    if (rightUp) {
      if (criminal.costumeNum < 12) {
        criminal.costumeNum++;
        criminal.switchCostumeToNum(criminal.costumeNum);
        criminal.changeX(2*xpx);
        if (criminal.x > 240*xpx) spriteGoTo(criminal, -240, -75);
        return;
      } else if (criminal.costumeNum == 12) {
        rightUp = false;
        rightDown = true;
        criminal.costumeNum = 11;
        criminal.switchCostumeToNum(11);
        criminal.changeX(2*xpx);
        if (criminal.x > 240*xpx) spriteGoTo(criminal, -240, -75);
        return;
      }
    }
    if (rightDown) {
      if (criminal.costumeNum > 8) {
        criminal.costumeNum--;
        criminal.switchCostumeToNum(criminal.costumeNum);
        criminal.changeX(2*xpx);
        if (criminal.x > 240*xpx) spriteGoTo(criminal, -240, -75);
        return;
      } else if (criminal.costumeNum == 8) {
        rightHopStarted = false;
        criminal.rightHop = false;
        rightDown = false;
        criminal.costumeNum = 0;
        criminal.switchCostumeToNum(0);
        criminal.changeX(2*xpx);
        if (criminal.x > 240*xpx) spriteGoTo(criminal, -240, -75);
        return;
      }
    }
  }
}

function restart() {
  document.getElementById('score').innerHTML = 'Score: 0';
  deleteAllClones();
  stage.switchBackdropToNum(0);
  pressSpace.show();
  criminal.show();
  criminal.switchCostumeToNum(1);
  removeExtraImages();
  removeExtraImages()
  document.getElementById('largeScore').style.visibility = 'hidden';
  document.getElementById('largeHighscore').style.visibility = 'hidden'
  document.getElementById('largeScore').innerHTML = 'Score: 0';
  document.getElementById('largeHighscore').innerHTML = 'Highscore: '+highscore;
  document.getElementById('score').style.visibility = 'visible';
  document.getElementById('highscore').style.visibility = 'visible';
  menuButton.hide();
  playAgain.hide();
  leftUp = false;
  leftDown = false;
  rightUp = false;
  rightDown = false;
  leftHopStarted = false;
  rightHopStarted = false;
  gameFinished = false;
  lastFrameTime = Date.now();
  speed = 1;
  score = 0;
  criminalX = 0;
  then = Date.now();
  clones = [];
  gameStarted = true;
  gameRunning = false;
  criminal.costumeNum = 0;
  spriteGoTo(criminal, -150, -75);
  gameOver.hide();
}
